27ZINE, Primeira Edição
======

[![License: CC0 1.0](https://licensebuttons.net/l/zero/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0)

Este projeto é um trabalho dos nobres senhores do 27chan.org. O 27chan não se responsabiliza pelo conteúdo desta revista eletrônica, e não é responsável pelas palavras proferidas por seus usuários sob a luz da liberdade de expressão.

Ademais, todo o conteúdo do 27chan.org não existe, é apenas a imaginação de várias mentes perturbadas.

## Conteúdo em produção

O conteúdo do 27ZINE será separado em seções, contendo os nomes das respectivas boards onde o conteúdo mais se encaixa. Ademais, os artigos serão organizados em ordem alfabética nas boards.

### /hq/
- hq de Pepe & Wojak [em progresso]

### /pol/
- artigo anti-Feto [em breve]
- artigo sobre "coisas erradas" a la Anarchist Cookbook [em breve]

### /sala/
- texto sobre a Roma Antiga [em breve]
- texto sobre Geografia [em breve]
- resumo e discussões a respeito dos principais livros do vestibular da Fuvest [em breve]
- "Giro e Capitalização: Erros comuns em organizações emergentes e como evitá-los" [em progresso]

### /arquivo/
- Censo 2017-1 [em progresso]
- uma seção para charts e guias visuais [em breve]

### /enter/
- "Invadindo WiFi com Reaver" [finalizado]
- "Lisp: Entendendo a Linguagem e Seus Dialetos" [em progresso]

### /N64/
- "Metroidvalias Que Você Deve Jogar Agora" [em progresso]

### /sala/
- "Fundamentos da Matemática" [em progresso]
