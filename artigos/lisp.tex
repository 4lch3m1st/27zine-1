\begin{centering}
  \section{Lisp: Entendendo a Linguagem e Seus Dialetos}
\end{centering}

\begin{multicols}{2}

% Introduzir o assunto
\noindent
\par Como qualquer bom programador que se preze, você provavelmente já deve ter ouvido falar de Lisp, ou do famigerado livro \textit{A Structure and Interpretation of Computer Programs}, de Abelson e Sussman, livro este que usa um dialeto desta linguagem como base para seu raciocínio.\\
\noindent
\par Surpreendentemente, apesar da aparente fama, a linguagem Lisp continua sendo uma das mais obscuras do mundo da programação. Sua sintaxe, a princípio, não oferece um cartão de boas vindas a programadores novatos, e não é à toa que um de seus mascotes é exatamente um alienígena!\\

\begingroup
\inlineimage{artigos/imagens/lisp1}
\footnotesize{Um dos mascotes do Lisp: Alienígena à primeira vista, mas muito funcional.}\\
\endgroup

\noindent
\par Mesmo com certa intimidação, Lisp é uma linguagem madura e muito usada no campo teórico, sendo valiosíssima em tópicos acadêmicos e até mesmo na introdução da programação a novatos.

% O que é Lisp, quais são os principais dialetos
\subsection{Mas o que é um Lisp?}
O termo \textbf{Lisp}, em si, é um acrônimo para a expressão \textit{List Processor}, que significa literalmente ``Processador de Listas''. Lisp foi criada por John McCarthy em 1960, como uma resposta direta à linguagem Fortran I, visando buscar uma simplificação de alguns processos. A ideia principal de Lisp está na sua simplicidade, girando em torno de uma simples estrutura de dados (uma lista). Lisp seria uma linguagem interpretada, dinâmica, e capaz de borrar a linha que separa a distinção entre dados e procedimentos de um programa de computador.\\
Nos anos que viriam, Lisp desempenhou um papel muito importante no desenvolvimento de programas de computador e modelos de máquinas, principalmente no ramo da inteligência artificial. As chamadas Máquinas Lisp, no entanto, se tornaram um conceito obsoleto, por estarem muito além do que a época poderia oferecer, no quesito performance.\\
Falar de Lisp como uma linguagem única e definitiva é cometer um crime contra a mesma. O Lisp original de McCarthy foi substituído e teve acréscimos notáveis, até chegar à sua versão 1.5, em 1964. A partir daí, a linguagem se subdividiu em seus muitos dialetos, e permaneceu fragmentada por um bom tempo até que, muitos anos depois, foram consolidados os principais dialetos, que englobavam o melhor de outros dialetos menores ou menos conhecidos.\\
Hoje, os principais dialetos de Lisp são:
\begin{itemize}
\item \textbf{Common Lisp.} Sem dúvida o mais completo, tendo sua primeira documentação publicada em 1984, e seu padrão ANSI estabelecido dez anos depois, em 1994. Possui diversas implementações e pode ser considerada a mais madura e a mais complexa, tendo englobado a maior quantidade de elementos de outros Lisps -- nas palavras da comunidade, é \textit{``fácil de usar, mas difícil de alcançar a proficiência''}.
\item \textbf{Emacs Lisp.} Provavelmente o dialeto de Lisp mais usado, mesmo que por acidente. É a linguagem utilizada, como o nome sugere, no editor de texto \textit{Emacs}, uma ferramenta altamente extensível (e também recomendada) e um excelente editor de código, principalmente para a maioria dos dialetos de Lisp, já que pode facilmente se tornar uma IDE para os mesmos. O poder de Emacs Lisp e do editor onde é executado mostram-se presentes nesta extensibilidade do \textit{Emacs}, que o torna capaz, também, tanto de funcionar como navegador web como de funcionar como gerenciador de janelas (em um sistema como Linux, por exemplo). É a linguagem recomendada para programadores aspirantes que visam usar o Emacs e/ou o Common Lisp no futuro.
\item \textbf{Scheme.} Possivelmente a mais simples de todas, e a menor em termos de especificações, Scheme é um Lisp flexível e que pode ser encaixado em praticamente qualquer lugar, dependendo da sua implementação. Criada por Steele e Sussman, em 1970, tem um subconjunto de sua especificação usado no livro \textit{A Structure and Interpretation of Computer Programs}. Scheme possui diversas especificações atualmente e pode, ainda que unificada por padrões, ser considerada fragmentada por alguns motivos, sendo o principal deles a falta de uma especificação para uso de pacotes; ou seja, cada implementação de Scheme lida com pacotes (bibliotecas) de uma forma diferente, ou simplesmente não lida com pacotes. Ainda assim, seu minimalismo (principalmente no que tange às especificações anteriores à famigerada \textbf{R7RS}, que alegadamente tira parte do minimalismo da linguagem) atrai programadores para o seu uso direto como linguagem de script embarcada em programas, tendo diversas implementações fáceis de serem utilizadas em programas C, C++ e similares.
\item \textbf{Clojure.} Outro dialeto madur
  o e com uma boa comunidade. Clojure se destaca por ser um Lisp executado diretamente na JVM, a \textit{Máquina Virtual Java}. Por este motivo, pode usufruir de pacotes feitos na linguagem Java, enquanto mantém a simplicidade e as características marcantes de um Lisp. Clojure também tem uma grande ênfase na imutabilidade de suas declarações, e a evangelização deste conceito garante uma otimização extra ao desenvolver programas nesta linguagem. Usando uma ferramenta como \textbf{Leiningen}, é possível criar até mesmo aplicativos para a plataforma Android, usando apenas Clojure.
\item \textbf{LFE.} Criado em 2007 por Robert Virding, LFE (sigla para \textit{Lisp-Flavored Erlang}) toma emprestado as características principais de um Lisp para funcionar como uma alternativa direta à sintaxe de Erlang, enquanto ainda se mantém fiel à filosofia desta última (imutável, funcional e orientada a erros -- filosofia \textit{``Let it Fail''} -- e a um modelo de atores e de \textit{concurrency}). LFE executa sobre a Beam, a Máquina Virtual de Erlang, e tem ganhado espaço com sua elegância e seu casamento perfeito entre um Lisp e um paradigma puramente funcional.
\end{itemize}

% Imagem do Racket
\begingroup
\inlineimage{artigos/imagens/lisp2}
\footnotesize{Racket, um dos principais dialetos de Scheme.}\\
\endgroup

Há, também, alguns ``sub-dialetos'' que merecem menções honrosas:

\begin{itemize}
  % Mais dialetos!
\item \textbf{Racket.} Surgindo como um dialeto de Scheme, Racket se tornou um dos dialetos mais utilizados e mais recomendados para iniciantes, tendo conquistado seu espaço principalmente pelas ferramentas que o acompanham. Racket inclui um excelente sistema de gerenciamento de pacotes, e uma IDE fantástica chamada \textit{Dr. Racket}. Além disso, o ponto mais marcante de Racket é a sua facilidade e seu incentivo à criação de extensões da linguagem, tendo diretivas para que o usuário possa criar sua própria linguagem, a partir de Racket. Também inclui, por padrão, suporte a outras linguagens, como o dialeto de Scheme usado no \textit{SICP} (MIT Scheme).
\item \textbf{ClojureScript.} Tendo ainda suas raízes na versatilidade e no uso da JVM, como sugerido no nome, ClojureScript busca trazer o mundo dos Lisps à web, sendo um dialeto específico capaz de ser \textit{crosspilado} (traduzido) para JavaScript. Tem ganhado seu espaço entre os \textit{clojuristas}, e até entre programadores web que buscam uma maior pureza de paradigmas no desenvolvimento.
\end{itemize}


\subsubsection{Características gerais dos dialetos de Lisp}
Dialetos de Lisp seguem certas características que distinguem-nos de quaisquer outras linguagens, garantindo sua sintaxe característica e o valor desta sintaxe.\\

Aspectos estéticos comuns de um Lisp são:
\begin{itemize}
\item O uso de parênteses, designando listas e incentivando uma visão em árvore do algoritmo;
\item O incentivo e o uso intenso de \textit{macros};
\item A escrita e a eventual indentação capaz de explicitar o próprio funcionamento e a estrutura do programa.
\end{itemize}

Estas características definem o principal descritor de um Lisp: a \textbf{homoiconicidade}.\\

% Imagem do GNU Guile
\begingroup
\inlineimage{artigos/imagens/lisp3}
\footnotesize{GNU/Guile, uma implementação de Scheme, é também capaz de ser estendido de forma a interpretar até mesmo linguagens com sintaxe diferente de um Lisp. Suporta Emacs Lisp, estando cotado para substituir o interpretador do próprio Emacs, e já tem seu suporte a Lua e ECMAScript em progresso.}\\
\endgroup

Ao analisar um código Lisp,

\noindent
% Vantagens
\paragraph{O melhor do paradigma declarativo}
\lipsum[1]

% Desvantagens
\noindent
\paragraph{Nem tudo que reluz é ouro}
\lipsum[1]


% Casos de estudo
\subsection{Exemplos de uso}
% fib(n), com C e LFE
\subsubsection{Calculando o \textit{n}-ésimo número da sequência de Fibonacci}
% YASWEG, com HTML e Guile Scheme
\subsubsection{Gerando uma página HTML estática com Scheme}

% Usos no mercado, atualmente
\subsection{O mercado e os Lisps}

% Conclusão


\end{multicols}
\clearpage
